<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("NAME_EXPORT_EXEL"),
	"DESCRIPTION" => GetMessage("DISCR_EXPORT_EXEL"),
	"SORT" => 30,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "mcart",
		"NAME" => GetMessage("MCART_NAME"),
		"SORT" => 10,
	),
);

?>
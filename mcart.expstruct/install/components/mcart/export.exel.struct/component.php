<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
global $USER;

$arResult['ACCESS'] = false;

if(!empty($arParams['GROUPS_WITH_ACCESS'])){
    $IDUser = $USER->GetID();
    $arUsersGroupsCur = CUser::GetUserGroup($IDUser);

    foreach($arUsersGroupsCur as $gr) {
        if (in_array($gr, $arParams['GROUPS_WITH_ACCESS'])) {
            $arResult['ACCESS'] = true;
            break;
        }
    }
}
if($arResult['ACCESS'] != true) {
    if ($USER->IsAdmin()) {
        $arResult['ACCESS'] = true;
    }
}


if($arResult['ACCESS'] == true) {
    if (!empty($_POST['export'])) {
        if (!empty($arParams['USER_FIELDS']) || !empty($arParams['USER_PROPERTY'])) {

            if (!in_array('NAME', $arParams['USER_FIELDS'])) {
                $arParams['USER_FIELDS'][] = 'NAME';
            }

            if (!in_array('LAST_NAME', $arParams['USER_FIELDS'])) {
                $arParams['USER_FIELDS'][] = 'LAST_NAME';
            }

            //����������� ����� ���� ����� � ���������� ��� ������� �������, ��� �� ������ ������ � ������� �������� �� ����������

            $userProp1 = CSocNetUser::GetFields();
            unset($userProp1["PASSWORD"]);

            foreach ($userProp1 as $key => $prop) {
                if (!in_array($key, $arParams['USER_FIELDS'])) {
                    unset($userProp1[$key]);
                }
            }

            $arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);

            $arFieldCheck = array();

            foreach ($arRes as $key => $val) {
                if (!in_array($key, $arParams['USER_PROPERTY'])) {
                    unset($arRes[$key]);
                } else {
                    $arFieldCheck[$val['FIELD_NAME']] = $val;
                    $arRes[$key] = $val['LIST_COLUMN_LABEL'];
                }

               /* if($key = "UF_DEPARTMENT"){
                    $arRes["SUB_TO"] = GetMessage("SUB_TO");
                }*/
            }

            if(!empty($arRes["UF_DEPARTMENT"]) && $arParams['SUB_TO_CHECKBOX'] == "Y"){
                $arRes["SUB_TO"] = GetMessage("SUB_TO");
            }

            //�������� ��� ����� ��������������

            $arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_5_SECTION", 0, LANGUAGE_ID);

            foreach ($arUF as $key => $val) {
                if (!in_array($key, $arParams['SECTION_PROPERTY'])) {
                    unset($arUF[$key]);
                } else {
                    $arUF[$key] = $val['LIST_COLUMN_LABEL'];
                }
            }

            $tmpArrM = array_merge($userProp1, $arRes);

            $arResult['ITEMS'][] = array_merge($tmpArrM, $arUF);

            //������� ��� �������� �����-�������

            //echo "<pre>";
            //print_r($arFieldCheck);
            //echo "</pre>";
            $arEnumIDs = array();
            foreach ($arFieldCheck as $field) {
                if ($field['USER_TYPE_ID'] == 'enumeration') {
                    $arEnumIDs[] = $field['ID'];
                }
            }

            $arEnumVal = array();
            $rsGender = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_ID" => $arEnumIDs,
            ));
            while ($arGender = $rsGender->GetNext()) {
                $arEnumVal[$arGender["ID"]] = $arGender["VALUE"];
            }


            //����������� ������������
            $iCount = 0;
            $arFieldsMain = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL');

            $arFields = array_merge($arFieldsMain, $arParams['SECTION_PROPERTY']);

            $rsSections = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), array('IBLOCK_ID' => 5), false, $arFields);
            $arSections = array();
            $arDopFieldsSec = array();
            while ($arSction = $rsSections->Fetch()) {
                //���������� ��� ������ �������� ������ �����������, ��� �� ������, ������� ������� ��� �����������, ��� �� ���������� �������� ���������
                if ($iCount < intval($arSction['DEPTH_LEVEL'])) {
                    //echo "khkjhkj";
                    $iCount = $arSction['DEPTH_LEVEL'];
                }


                $arSections[$arSction['ID']] = $arSction;
                $idSec = $arSction['ID'];
                foreach ($arSction as $key => $secF) {
                    if (in_array($key, $arFieldsMain)) {
                        unset($arSction[$key]);
                    }
                }
                //������ �� ��� �����, ������� ���� ������� � ������
                $arDopFieldsSec[$idSec] = $arSction;
            }
            //echo $iCount;
            //���������� ���� "Function responsibility" � �������������� ����� ������ ��������, ���� ��� ������
            $a = 1;
            while ($a <= $iCount) {
                foreach ($arSections as $key => $dep) {
                    if (!empty($dep['UF_FUNC_RESP'])) {
                        foreach ($arSections as $key2 => $dep2) {
                            if (($dep2['IBLOCK_SECTION_ID'] == $key) && empty($dep2['UF_FUNC_RESP'])) {
                                $arSections[$key2]['UF_FUNC_RESP'] = $dep['UF_FUNC_RESP'];
                                $arDopFieldsSec[$key2]['UF_FUNC_RESP'] = $dep['UF_FUNC_RESP'];
                            }
                        }
                    }
                    //$arDopFieldsSec[$key] = $dep;
                }
                $a++;
            }
            //echo "<pre>";
            //print_r($arSections);
            //echo "</pre>";

            //������ ������������� � ������ ����������

            $arSectionsChack = $arSections;
            foreach ($arSectionsChack as $key => $degChack) {
                if (in_array($key, $arParams['SECTION_EXEP'])) {
                    unset($arSectionsChack[$key]);
                    foreach ($arSectionsChack as $key2 => $degChack2) {
                        if ($degChack2['IBLOCK_SECTION_ID'] == $key) {
                            unset($arSectionsChack[$key2]);
                        }
                    }
                }
            }
            //echo "<pre>";
            //print_r($arSectionsChack);
            //echo "</pre>";

            foreach ($arSections as $key => $sec) {

                //$name = array();
                $name = $sec['NAME'];
                $count = $sec['DEPTH_LEVEL'];
                $id = $sec['IBLOCK_SECTION_ID'];
                while ($count > 1) {
                    $tmp = $arSections[$id];
                    $id = $tmp['IBLOCK_SECTION_ID'];
                    $name = $tmp['NAME'] . "/" . $name;
                    $count--;
                }

                //$name = pathToDep($arSections, $sec, $arr);
                $arSections[$key]['NAME_FULL'] = $name;
            }

            //����������� ������������� � ���������� ������������� � ����������� �������������� ��� ������ �������� �������
            $flagNoID = false; //����� ���� ��������� ���� id � ������� �����
            if (!in_array('ID', $arParams['USER_FIELDS'])) {
                $flagNoID = true;
                $arParams['USER_FIELDS'][] = 'ID'; //����� ����������� id ��� ������� �������� � ����������
            }
            $arUserCheck = array();
            $arDB = CUser::GetList(($by = "personal_country"), ($order = "desc"), array("ACTIVE" => "Y"), array("SELECT" => $arParams['USER_PROPERTY'], "FIELDS" => $arParams['USER_FIELDS']));
            while ($arP = $arDB->Fetch()) {

                $arUserCheck[$arP['ID']] = $arP;
                if ($flagNoID == true) {
                    unset($arP['ID']);
                }
                if (!empty($arP['UF_DEPARTMENT'])) {

                    foreach ($arP['UF_DEPARTMENT'] as $key => $depID) {
                        if (empty($arSectionsChack[$depID])) {
                            unset($arP['UF_DEPARTMENT'][$key]);
                        }
                    }

                    if (CModule::IncludeModule('intranet')) {
                        $arSubTo = CIntranetUtils::GetDepartmentManager($arP["UF_DEPARTMENT"], $arP["ID"], true);
                        //echo "<pre>";
                        //print_r($arSubTo);
                        //echo "</pre>";
                        if(!empty($arResult['ITEMS'][0]["SUB_TO"])) {
                            //$arSubTmp = implode(", ", $arSubTo);
                            $flagfirst = true;
                            foreach ($arSubTo as $MenDep) {
                                if($flagfirst == true) {
                                    $arP["SUB_TO"] = $MenDep["NAME"]." ".$MenDep["LAST_NAME"];
                                    $flagfirst = false;
                                } else {
                                    $arP["SUB_TO"] .= ", ".$MenDep["NAME"]." ".$MenDep["LAST_NAME"];
                                }

                            }
                        }
                    }

                    $cDep = count($arP['UF_DEPARTMENT']);

                    if ($cDep == 0) {
                        $arP['UF_DEPARTMENT'] = "";
                        foreach ($arParams['SECTION_PROPERTY'] as $parSec) {
                            $arP[$parSec] = "";
                        }
                        $arResult['ITEMS'][] = $arP;
                    }

                    //echo "<pre>";
                    //print_r($arP);
                    //echo "</pre>";

                    if ($cDep > 1) {
                        //$cDepNow = 0;
                        foreach ($arP['UF_DEPARTMENT'] as $dep) {
                            //$cDepNow++;
                            $arTmp = array();
                            $arTmp = $arP;
                            if (!empty($arSections[$dep]['NAME_FULL'])) {

                                $arTmp['UF_DEPARTMENT'] = $arSections[$dep]['NAME_FULL'];

                                //$arTmp['UF_DEPARTMENT']
                                foreach ($arDopFieldsSec[$dep] as $key => $dopFiSec) {
                                    $arTmp[$key] = $dopFiSec;
                                }
                                $arResult['ITEMS'][] = $arTmp;
                            } //else {
                            // if($cDepNow == $cDep) {
                            //     $arTmp['UF_DEPARTMENT'] = "";
                            // }
                            //}
                        }
                    } else {
                        foreach ($arP['UF_DEPARTMENT'] as $dep) {
                            $arTmp = array();
                            $arTmp = $arP;
                            // if (!empty($arSections[$dep]['NAME_FULL'])) {
                            //$arTmp['ID_SEC'] = $dep;
                            $arTmp['UF_DEPARTMENT'] = $arSections[$dep]['NAME_FULL'];
                            foreach ($arDopFieldsSec[$dep] as $key => $dopFiSec) {
                                $arTmp[$key] = $dopFiSec;
                            }
                            $arResult['ITEMS'][] = $arTmp;
                            //}
                        }
                    }
                } else {
                    //$arP['ID_SEC'] = "";
                    $arP['UF_DEPARTMENT'] = "";
                    foreach ($arParams['SECTION_PROPERTY'] as $parSec) {
                        $arP[$parSec] = "";
                    }
                    $arResult['ITEMS'][] = $arP;
                }

            }

            //echo "<pre>";
            //print_r($arResult);
            //echo "</pre>";
            //����������� �������� ������� ����� ��� ������ � �������� � ����������
            foreach ($arResult['ITEMS'] as $keyItem => $arItems) {
                if ($keyItem != 0) {
                    foreach ($arItems as $key => $par) {
                        if ($key != 'UF_DEPARTMENT') {
                            if ($arFieldCheck[$key]['MULTIPLE'] == 'Y') {
                                if ($arFieldCheck[$key]['USER_TYPE_ID'] == 'employee') {
                                    foreach ($par as $key2 => $item) {
                                        if (!empty($arUserCheck[$item]['NAME']) && !empty($arUserCheck[$item]['LAST_NAME'])) {
                                            $par[$key2] = $arUserCheck[$item]['NAME'] . " " . $arUserCheck[$item]['LAST_NAME'];
                                        } else if (!empty($arUserCheck[$item]['NAME'])) {
                                            $par[$key2] = $arUserCheck[$item]['NAME'];
                                        } else if (!empty($arUserCheck[$item]['LAST_NAME'])) {
                                            $par[$key2] = $arUserCheck[$item]['LAST_NAME'];
                                        } else {
                                            $par[$key2] = "noname";
                                        }
                                    }
                                    $arResult['ITEMS'][$keyItem][$key] = implode(', ', $par);
                                    //} else if($arFieldCheck[$key]['USER_TYPE_ID'] == 'enumeration'){

                                    //}
                                } else if ($arFieldCheck[$key]['USER_TYPE_ID'] == 'enumeration') {
                                    foreach ($par as $key2 => $item) {
                                        $par[$key2] = $arEnumVal[$item];
                                    }
                                    $arResult['ITEMS'][$keyItem][$key] = implode(', ', $par);
                                } else {
                                    $arResult['ITEMS'][$keyItem][$key] = implode(', ', $par);
                                }
                            } else {
                                if ($arFieldCheck[$key]['USER_TYPE_ID'] == 'enumeration') {
                                    $arResult['ITEMS'][$keyItem][$key] = $arEnumVal[$par];
                                }
                            }
                            //echo "<pre>";
                            //print_r($par);
                            //echo "</pre>";
                        }
                    }
                }
            }

            //echo "<pre>";
            //print_r($arResult);
            //echo "</pre>";

        }

    }
}
/*$arDB = CUser::GetList(($by = "personal_country"), ($order = "desc"), array(), array("SELECT" => $arParams['USER_PROPERTY'], "FIELDS" => $arParams['USER_FIELDS']));
while ($arP = $arDB->Fetch()) {
    $arrr[] = $arP;
}
echo "<pre>";
print_r($arrr);
echo "</pre>";*/

$this->IncludeComponentTemplate();
?>


<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!CModule::IncludeModule("socialnetwork"))
    return false;

$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
$userProp = array();
if (!empty($arRes))
{
    foreach ($arRes as $key => $val)
        $userProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}
$userProp1 = CSocNetUser::GetFields();
unset($userProp1["PASSWORD"]);

$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("IBLOCK_5_SECTION", 0, LANGUAGE_ID);

$secProp = array();

foreach ($arUF as $key => $val)
    $secProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);

$exDep = array();

//$arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // ������� �������� ��� ����� ����������
$rsSectEx = CIBlockSection::GetList(array('left_margin' => 'asc'), array("IBLOCK_ID" => 5));
while ($arSect = $rsSectEx->GetNext())
{
    $exDep[$arSect['ID']] = $arSect['NAME'];
}
$arUsersGroups = array();
$rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array());
while($arGroups = $rsGroups->Fetch())
{
    if($arGroups['ID'] != 1)
        $arUsersGroups[$arGroups['ID']] = $arGroups['NAME'];

}

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS"  =>  array(
        "USER_FIELDS" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("SONET_USER_FIELDS"),
            "TYPE" => "LIST",
            "VALUES" => $userProp1,
            "MULTIPLE" => "Y",
            "DEFAULT" => array(),
        ),
        "USER_PROPERTY" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("SONET_USER_PROPERTY"),
            "TYPE" => "LIST",
            "VALUES" => $userProp,
            "MULTIPLE" => "Y",
            "DEFAULT" => array(),
        ),
        "SECTION_PROPERTY" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("SONET_SECTION_PROPERTY"),
            "TYPE" => "LIST",
            "VALUES" => $secProp,
            "MULTIPLE" => "Y",
            "DEFAULT" => array(),
        ),
        "SECTION_EXEP" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("SECTION_EXEP"),
            "TYPE" => "LIST",
            "VALUES" => $exDep,
            "MULTIPLE" => "Y",
            "DEFAULT" => array(),
        ),
        "GROUPS_WITH_ACCESS" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("GROUPS_WITH_ACCESS"),
            "TYPE" => "LIST",
            "VALUES" => $arUsersGroups,
            "MULTIPLE" => "Y",
            "DEFAULT" => array(),
        ),
        "SUB_TO_CHECKBOX" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("SUB_TO_CHECKBOX"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
	),
);
?>

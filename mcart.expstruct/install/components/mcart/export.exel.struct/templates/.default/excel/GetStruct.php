<?php
global $APPLICATION;

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
$sheet = $excel->getActiveSheet();
$sheet->setTitle('План по товарным группам');

//$sheet->setCellValueByColumnAndRow(0, 1, "Товарная группа");
//$sheet->setCellValueByColumnAndRow(1, 1, "План");
//$sheet->setCellValueByColumnAndRow(2, 1, "Факт");
//$sheet->setCellValueByColumnAndRow(3, 1, "Процент выполнения");

$rowIndex = 1;
foreach ($statistics->GetGroupPlans() as $groupPlan)  {
    $sheet->setCellValueByColumnAndRow(0, $rowIndex, $groupPlan->getGroup());
    $sheet->setCellValueByColumnAndRow(1, $rowIndex, $groupPlan->getPlan()->getPlan());
    $sheet->setCellValueByColumnAndRow(2, $rowIndex, $groupPlan->getPlan()->getActual());
    $sheet->setCellValueByColumnAndRow(3, $rowIndex, $groupPlan->getPercent());
    $rowIndex++;
}


$APPLICATION->RestartBuffer();

ob_end_clean();
header( "Content-type: application/vnd.ms-excel" );
header('Content-Disposition: attachment; filename="План по товарным группам.xlsx"');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$writer->save('php://output');
exit();
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
//echo $arResult['ACCESS'];
if($arResult['ACCESS'] == true) {
    if (!empty($_POST['export'])) {
        set_include_path($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/mcart/export.exel.struct/ExcelAPI/PHPExcel/");
        require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/mcart/export.exel.struct/ExcelAPI/PHPExcel.php";
        //require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/mcart/export.exel.struct/ExcelAPI/PHPExcel/Writer/Excel5.php";
        global $APPLICATION;

        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);

        //$sheet->setTitle('���� �� �������� �������');

        $j = 1;
        //$excel->getActiveSheet()->setCellValueByColumnAndRow($i, $j, iconv(strtolower(SITE_CHARSET), 'utf-8',$ar));
        foreach ($arResult['ITEMS'] as $arItems) {
            //$excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, iconv(strtolower(SITE_CHARSET), 'utf-8',"asdasdasdasd"));
            $i = 0;
            foreach ($arItems as $arField) {
                if (strtolower(SITE_CHARSET) != 'utf-8')
                    $excel->getActiveSheet()->setCellValueByColumnAndRow($i, $j, iconv(strtolower(SITE_CHARSET), 'utf-8', $arField));
                else
                    $excel->getActiveSheet()->setCellValueByColumnAndRow($i, $j, $arField);
                $i++;

            }
            $j++;
        }


        $APPLICATION->RestartBuffer();


        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=export.xlsx");
        header("Content-Transfer-Encoding: binary ");

        $writer = new PHPExcel_Writer_Excel2007($excel);
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');


        exit();
    }


?>

<form method="post">
    <input name="export" type=submit value="Export">
</form>

<?}?>
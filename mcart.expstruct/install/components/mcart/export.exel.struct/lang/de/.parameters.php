<?
$MESS ['SONET_USER_FIELDS'] = "User's Properties";
$MESS ['SONET_USER_PROPERTY'] = "User's Additional Properties";
$MESS ['SONET_SECTION_PROPERTY'] = "Department's Additional Properties";
$MESS ['SECTION_EXEP'] = "Departments exceptions";
$MESS ['GROUPS_WITH_ACCESS'] = "Groups with access";
$MESS ['SUB_TO_CHECKBOX'] = "Display the 'Subordinate To' field? (Only works if the 'Departments' field is selected)";
?>